<h1>Raw Dataset</h1>

<p>Path : data/raw_dataset.csv</p>

Please, Cite the paper as,

`C Sitaula, A Basnet, A Mainali and TB Shahi, "Deep Learning-based Methods for Sentiment Analysis on Nepali COVID-19-related Tweets", 
Computational Intelligence and Neuroscience, forthcoming, 2021`